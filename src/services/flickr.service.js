import * as $ from 'jquery';

function search(str) {
    if (str==null) str = 'eagle';
    let url = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1&api_key=a4893081e1e2584a6bf88a2ed408ef5c&text='+str;
    return $.getJSON(url)
      .then(( results ) => { 
          return results;
    });
}

export default { search }