import React, { Component } from 'react';
import './App.css';
import * as $ from 'jquery';
import flickr from './services/flickr.service.js';

class App extends Component {

  constructor(props) {
    super(props);
    // default initial state
    this.state = {
      photos: {'page':1,'pages':10,'perpage':10,'total':100, 'photo': []}, 
      index: 0, 
      galleryIndex:0,
      search: ''
    };
  }

  // get data from and apply window bindings
  componentDidMount() {
    this.getPhotos();
    $(window).on('resize', () => {
        this.resetSlideshowWidth();
    });
  }

  // get photos 
  getPhotos(str) {
    flickr.search(str).then((results)=> {
      // update the state with results from Flickr api 
      this.setState({'photos':results.photos});
      // recalculate slideshow width  
      this.resetSlideshowWidth();
    });
  }

  resetSlideshowWidth(){
    // set scroller width
    let viewportWidth =  $(window).width();
    // set the scroller width
    $('#slideshow .scroller').width(100 * viewportWidth);
    // set slide width
    $('#slideshow .slide').width(viewportWidth);
    // slide to current slide
    this.toCurrentSlide();

    // set gallery scroller width
    let widthSum = 0;
    // sum slides width as they may be different
    $('#gallery .scroller .slide').each(function(){
      widthSum += parseInt( $(this).width(), 10);
    });
    // set gallery scroller width
    $('#gallery .scroller').width(widthSum);
  }

  toCurrentSlide() {
    let $slideshow = $('#slideshow');
    let index = this.state.index;
    let left = (index * $('#slideshow .slide:first').width());
    $slideshow.animate({scrollLeft : left+'px'});
  }

  previousSlide() {
    console.log(this.state.index);
    let $slideshow = $('#slideshow');
    let index = this.state.index - 1;
    let left = (index * $('#slideshow .slide:first').width());
    $slideshow.animate({scrollLeft : left+'px'});
    this.setState({'index':parseInt(index,10)});
  }

  nextSlide() {
    let $slideshow = $('#slideshow');
    let index = this.state.index + 1;
    let left = (index * $('#slideshow .slide:first').width());
    $slideshow.animate({scrollLeft : left+'px'});
    this.setState({'index':parseInt(index,10)});
  }

  nextSet(event) {
    let $gallery = $('#gallery');
    let galWidth = $gallery.width(); 
    let thumbWidth = $('#gallery .slide:first').width();
    let n = Math.floor(galWidth / thumbWidth);
    let left = (n * thumbWidth) + parseInt(this.state.galleryIndex, 10);
    $gallery.animate({scrollLeft : left + 'px'});
    this.setState({'galleryIndex':parseInt(left, 10)});
  }

  previousSet(event) {
    let $gallery = $('#gallery');
    let galWidth = $gallery.width(); 
    let thumbWidth = $('#gallery .slide:first').width();
    let n = Math.floor(galWidth / thumbWidth);
    let left = parseInt(this.state.galleryIndex, 10) - (n * thumbWidth);
    $gallery.animate({scrollLeft : left + 'px'});
    this.setState({'galleryIndex':parseInt(left, 10)});
  }
  
  // thumbnail click handler
  slideClick(item, event) {
    // block click events on large picture
    //if (!$(event.target).closest('.slides-container').is('#slideshow'))
    
      // select elements
      var $slideshow = $('#slideshow');
      // get index of clicked element
      let index = $(event.target).closest('.slide').attr('data-index');
      // calculate element position
      let left = (index * $('#slideshow .slide:first').width());
      // animate the scroller
      $slideshow.animate({scrollLeft : left+'px'});
      // store current slide index
      this.setState({'index':parseInt(index,10)});
    
  }

  handleSearch(event) {
    this.setState({search: event.target.value});
    this.setState({index: 0});
    this.setState({galleryIndex: 0});
  }

  // search function
  search(event) {
    // prevent form submit
    event.preventDefault();
    // todo: sanitize user input
    this.getPhotos(this.state.search);
  }

  render() {
    const photos = this.state.photos.photo.map((item, i) => (
      <div className="slide flex flex-center" key={item.id} data-key={item.id} data-index={i} onClick={(event) => this.slideClick(item, event)}>
        <img src={ 'https://farm' + item.farm + '.staticflickr.com/' + item.server + '/' + item.id + '_' + item.secret + '.jpg' } className="photo" alt="" />
      </div>
    ));

    return (
      <div id="layout-content" className="App flex flex-column">
        <header className="App-header">
          <form onSubmit={(event) => this.search(event)}>
            <input className="App-search-input" onChange={(event) => this.handleSearch(event)} />
          </form>
        </header>
        <div id="slideshow" className="slides-container lg">
          <div className="scroller">
              { photos }
          </div>
          <div className={"arrow arrow-left" + (this.state.index <= 0 ? ' arrow-hidden' : '' )} onClick={(event) => { this.previousSlide(event); }}>❮</div>
          <div className={"arrow arrow-right" + (this.state.index >= 100 ? ' arrow-hidden' : '' )} onClick={(event) => { this.nextSlide(event); }}>❯</div>
        </div>
        <div id="gallery" className="slides-container sm">
            <div className="scroller">
                { photos }
            </div>
            <div className={"arrow-sm arrow-left-sm" + (this.state.galleryIndex <= 0 ? ' arrow-hidden' : '' )} onClick={(event) => { this.previousSet(event); }}>❮</div>
            <div className={"arrow-sm arrow-right-sm" + (this.state.galleryIndex >= 100 ? ' arrow-hidden' : '' )} onClick={(event) => { this.nextSet(event); }}>❯</div>
        </div>
      </div>
    );
  }
}

export default App;
